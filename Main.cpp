#include "Node.hpp"
#include <iostream>
/*
 *	wdd
 *	start January 5th 2021
 *
 * */

int main () {


	nptr n = nptr(new Node());
	nptr t = nptr(new Node());
	nptr x = nptr(new Node());
	nptr y = nptr(new Node());
	nptr z = nptr(new Node());


	std::cout << "====================\nwhats what\n";
	std::cout << "n is " << n << "\n";
	std::cout << "t is " << t << "\n";
	std::cout << "x is " << x << "\n";
	std::cout << "y is " << y << "\n";
	std::cout << "z is " << z << "\n";
	std::cout << "====================\nshow map\n";



	n->AddConnection(t);
	n->AddConnection(x);
	x->AddConnection(y);
	x->AddConnection(z);
	n->AddConnection(z);

	n->print();

	std::cout << "====================\nafter droping x\n";
	n->DropConnection(x);
	n->print();


	return 1;
}
