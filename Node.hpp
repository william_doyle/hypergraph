#pragma once
#include <stack>
#include <ostream>
#include <memory>
/*
 *	William Doyle
 *  January 5th 2021
 *  Node.hpp
 *  A node in a hypergraph (nodes can have any number of connections)
 *
 * */


#define nptr std::shared_ptr<Node>
class Node {
	public:
		Node();								
		~Node();						
		nptr AddConnection(nptr);
		bool DropConnection(nptr);

		std::stack<nptr>* connections;	// all our current connections
		unsigned connectionCount;
		
		void print();

};
