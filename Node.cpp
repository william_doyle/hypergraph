#include "Node.hpp"
#include <iostream>
#include <sstream>
/*	William Doyle
 *	January 5th 2020
 *
 *
 * */


/**
 *		wdd
 *		January 5th 2021
 *		zero arg constructor
 * */
Node::Node() {
	this->connections = new std::stack<nptr>();
}

/*
 *	wdd
 *	january 5th 2021
 *	destructor of node
 *
 * */
Node::~Node(){
	delete this->connections;	
}


/*
 *	wdd
 *	january 5th 2021
 *	AddConnection
 * */
nptr Node::AddConnection(nptr nToAdd){
	this->connections->push(nToAdd);
	this->connectionCount += 1;
	return nToAdd;
}


/**
 *	wdd
 *	jan 5th 2021
 *	drop connection
 *
 * */
bool Node::DropConnection(nptr nToDrop){
	std::stack<nptr>* unprocessed = this->connections;
	this->connections = new std::stack<nptr>();
	
	bool found = false;

	while (unprocessed->empty() == false){
	
		if (unprocessed->top() == nToDrop){
			unprocessed->pop();
			found = true;
			continue;
		}
		this->connections->push(unprocessed->top());
		unprocessed->pop();

	}

	delete unprocessed;
	this->connectionCount -= 1;	
	return found;
}


/**
 *	William Doyle
 *	January 5th 2021
 *	print()
 *	returns void
 *	prints this node and its connections
 * */
void Node::print(){
	
	static short indntlvl = 0;											// indent level
	std::stringstream ss;												// holds tabs for indent
	for (short i = 0; i < indntlvl; i++)								// put tabs in ss
		ss << "\t";
	
	std::cout << ss.str() << this << "[\n";								// output tabs and address of this node
	indntlvl++;															// increase indent level

	std::stack<nptr>* unprocessed = this->connections;					// our connections are unprocessed
	this->connections = new std::stack<nptr>();							// empty stack for processed connections

	while (unprocessed->empty() == false){

		if (unprocessed->top()->connections->empty() == true){
			std::stringstream innerss;
	
			for (short i = 0; i < indntlvl; i++)
				innerss << "\t";
			std::cout << innerss.str() << unprocessed->top() << ",\n";

		}
		else {
			unprocessed->top()->print();
		}

		this->connections->push(unprocessed->top());
		unprocessed->pop();
	}

	indntlvl --;
	std::cout << "\n" << ss.str() << "]";
	delete unprocessed;
	std::cout << "\n";
	return;

};
